.PHONY: docs ecs-deploy-tst ecs-deploy-acc ecs-deploy-prd ecs-deploy

PROJECT = parking_slots
SHELL = /bin/bash

default: help

# COLORS
GREEN  := $(shell tput -Txterm setaf 2)
YELLOW := $(shell tput -Txterm setaf 3)
WHITE  := $(shell tput -Txterm setaf 7)
RESET  := $(shell tput -Txterm sgr0)

TARGET_MAX_CHAR_NUM=20

## Show help
help:
	@echo ''
	@echo 'Usage:'
	@echo '  ${YELLOW}make ${GREEN}<target>${RESET}'
	@echo ''
	@echo 'Targets:'
	@awk '/^[a-zA-Z\-\_0-9]+:/ { \
		helpMessage = match(lastLine, /^## (.*)/); \
		if (helpMessage) { \
			helpCommand = substr($$1, 0, index($$1, ":")); \
			helpMessage = substr(lastLine, RSTART + 3, RLENGTH); \
			printf "  ${YELLOW}%-$(TARGET_MAX_CHAR_NUM)s ${GREEN}%s${RESET}\n", helpCommand, helpMessage; \
		} \
	} \
	{ lastLine = $$0 }' $(MAKEFILE_LIST)
	@echo ''

## Cleanup the codebase from temp files
clean:
	@find . -name '*.pyc' | xargs rm -rf
	@find . -name '*.egg-info' | xargs rm -rf

## Install back-end requirements
requirements:
	@pip install -e .[dev]

## Create migrations
migrations:
	python manage.py makemigrations

## Run migrations
migrate:
	python manage.py migrate

## Run complete test suite, with new DB
test:
	py.test tests/

## Run complete test suite, with new DB
docker-test:
	docker exec $(PROJECT) py.test -s code/tests

## Run tests and open coverage report
coverage:
	py.test --cov=src tests/ --cov-report html --cov-config .coveragerc

## Check your code for style errors
lint:
	@isort --check-only --recursive src/ tests/
	@flake8 src/ tests/

## Sort python imports automatically, be carefull!
fmt:
	@isort --recursive src tests
	@black -l 79 -S --target-version=py37 src tests

## dump all test data from the DB. Output - "test.json" file
dump-test:
	python manage.py dumpdata --exclude auth.permission --exclude contenttypes --exclude admin --exclude sessions > fixtures/test.json

## load data from the DB dump file. Args: <filename>
load_data:
	docker exec $(PROJECT) manage.py loaddata code/fixtures/${filename}

## load data from the DB dump file. Args: <filename>
load_test_data:
	docker exec $(PROJECT) manage.py loaddata code/fixtures/test.json

## Clean up all previous generated packages
clean_dist:
	rm -rf ./dist/*

## Build installable Python package
package: clean_dist
	python setup.py sdist

## Build Docker container
build: package
	docker build -t $(PROJECT) .

## Build and run docker container locally
build-and-run: build
	docker-compose up -d --build

## Build and run docker container locally, load test data
init:
	make build-and-run
	docker-compose restart web
	make load_test_data

## Run docker container locally
run:
	docker-compose up -d --build

## Run docker container locally
stop:
	docker-compose stop

## drop db -> recreate db -> run services -> load test data
restart_db:
	make stop
	docker-compose rm -vf db
	make init

## SSH into local (running) docker container
local-docker-ssh:
	docker exec -it $(PROJECT) /bin/sh

## Destroy and remove local docker container
local-docker-destroy:
	@docker kill $(PROJECT)
	@docker rm -f $(PROJECT)
	@docker rmi -f $(PROJECT)


