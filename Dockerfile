FROM python:3.7-alpine
LABEL maintainer="Andrew Voronov <voronov.a.g@gmail.com>"

WORKDIR /opt/parking_slots/
ENV DJANGO_ENV_FILE /opt/parking_slots/etc/application.settings

RUN apk update && \
    apk add curl mailcap gettext make postgresql-dev musl-dev gcc linux-headers libjpeg-turbo-dev zlib-dev libffi-dev

ADD ./dist /tmp
ADD ./docker /opt/parking_slots/etc

RUN chmod +x /opt/parking_slots/etc/entrypoint.sh && \
    pip install --upgrade uwsgi `find /tmp/ -name '*.tar.gz' | tail -1`

EXPOSE 80

ENTRYPOINT /opt/parking_slots/etc/entrypoint.sh
