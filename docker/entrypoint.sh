#!/bin/sh

#: Configure application
manage.py collectstatic --noinput
manage.py migrate --noinput

#: Lastly, Run uWSGI webserver
uwsgi --ini /opt/parking_slots/etc/uwsgi.ini
