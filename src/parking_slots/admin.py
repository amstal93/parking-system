from django.contrib import admin

from parking_slots import models


class ParkingLotAdmin(admin.ModelAdmin):
    ordering = ('name',)
    list_display = ('get_lot_owner', 'name')

    def get_lot_owner(self, obj):
        return obj.lot_owner.email

    get_lot_owner.short_description = "Owner"


class ParkingLotDateAdmin(admin.ModelAdmin):
    ordering = ('date',)
    list_display = ('get_parking_lot', 'date')

    def get_parking_lot(self, obj):
        return obj.parking_lot.name

    get_parking_lot.short_description = "Parking Lot"


class TimeSlotUserAdmin(admin.ModelAdmin):
    ordering = ('email', 'phone_number')
    list_display = ('name', 'email', 'phone_number')


class ParkingTimeSlotAdmin(admin.ModelAdmin):
    ordering = ('start_time',)
    list_display = (
        'get_slot_name',
        'get_slot_date',
        'get_slot_user',
        'start_time',
        'end_time',
        'booked',
    )

    def get_slot_name(self, obj):
        return obj.slot_date.parking_lot.name

    get_slot_name.short_description = "Slot Name"

    def get_slot_date(self, obj):
        return obj.slot_date.date

    get_slot_date.short_description = "Date"

    def get_slot_user(self, obj):
        if obj.slot_user:
            return obj.slot_user.email
        return "-"


admin.site.register(models.ParkingLot, ParkingLotAdmin)
admin.site.register(models.ParkingLotDate, ParkingLotDateAdmin)
admin.site.register(models.TimeSlotUser, TimeSlotUserAdmin)
admin.site.register(models.ParkingTimeSlot, ParkingTimeSlotAdmin)
