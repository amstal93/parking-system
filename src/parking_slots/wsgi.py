import os

from wsgi_basic_auth import BasicAuth

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'parking_slots.settings')
os.environ.setdefault('DJANGO_CONFIGURATION', 'Development')
os.environ.setdefault('WSGI_AUTH_EXCLUDE_PATHS', '/healthcheck/;/static/')

from configurations.wsgi import get_wsgi_application  # noqa isort:skip

application = BasicAuth(get_wsgi_application())
