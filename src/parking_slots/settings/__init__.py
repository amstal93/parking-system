from .envs import BaseDevelopment, Production, Testing  # noqa

try:
    from .local import LocalSettings
except ImportError:
    Development = BaseDevelopment
else:

    class Development(LocalSettings, BaseDevelopment):
        pass
