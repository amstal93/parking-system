# parking_slots settings

Settings for this project are managed via `django-configurations`, this allows us to configure settings via `Python` 
classes, which makes it easier and more `DRY` to extend and inherit settings between different environments.


## Local settings override

Overriding settings locally can easily be done by creating a `local.py` file which can contain a plain `Python` class 
called `LocalSettings` that allows you to add, change or modify settings

```python

    class LocalSettings:
        @property
        def INSTALLED_APPS(self):
            return super().INSTALLED_APPS + [
                'debug_toolbar'
            ]

```

### Docker settings

To run the docker locally you have to replace the LocalSettings class above in your local.py file.
To the one below.

```python
    class LocalSettings:
        DATABASES = {
            'default': {
                'ENGINE': 'django.db.backends.postgresql_psycopg2',
                'NAME': 'toogethr',
                'PASSWORD': 'postgres',
                'USER': 'postgres',
                'HOST': 'db',
                'PORT': 5432,
            }
        }

```

or 

```python
import dj_database_url


class LocalSettings:
    DATABASES = {
        'default': dj_database_url.parse(
            'pgsql://postgres:postgres@db:5432/toogethr'
        )
    }
```
