from django.apps import AppConfig


class Parking_slotsAppConfig(AppConfig):
    name = 'parking_slots'
    label = 'parking_slots'
    verbose_name = "parking_slots"
