from django.apps import AppConfig


class UserAppConfig(AppConfig):
    name = 'parking_slots.apps.users'
    label = 'users'
    verbose_name = "Users"
