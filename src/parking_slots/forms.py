import re

from django import forms
from django.utils import timezone as tz

from .models import ParkingTimeSlot, TimeSlotUser


class TimeSlotForm(forms.Form):
    name = forms.CharField(max_length=50)
    phone_number = forms.CharField(
        max_length=50,
        widget=forms.TextInput(attrs={'placeholder': '+31612344555'}),
    )
    email = forms.EmailField(
        widget=forms.TextInput(attrs={'placeholder': 'example@example.com'})
    )
    start_time = forms.TimeField(
        label='Start time',
        widget=forms.TimeInput(format='%H:%M', attrs={'placeholder': '09:00'}),
    )
    end_time = forms.TimeField(
        label='End time',
        widget=forms.TimeInput(format='%H:%M', attrs={'placeholder': '18:00'}),
    )

    date = forms.DateField(widget=forms.HiddenInput(), initial=tz.now().date())
    owner_id = forms.IntegerField(widget=forms.HiddenInput())

    def clean_phone_number(self):
        value = self.cleaned_data['phone_number']
        if not re.match(r"^\+31\d{9}$", value):
            raise forms.ValidationError(
                f"{value} is not a valid international format phone number"
            )
        return value

    def clean(self):
        cleaned_data = super().clean()
        if self.errors:
            return cleaned_data

        start_time = cleaned_data['start_time']
        end_time = cleaned_data['end_time']

        if end_time <= start_time:
            raise forms.ValidationError(
                "Please fill correct time slot period."
            )

        name = cleaned_data['name']
        phone_number = cleaned_data['phone_number']
        email = cleaned_data['email']

        time_slot = ParkingTimeSlot.objects.filter(
            slot_date__date=cleaned_data['date'],
            slot_date__parking_lot__lot_owner_id=cleaned_data['owner_id'],
            start_time__lte=start_time,
            end_time__gte=end_time,
            booked=False,
        ).first()
        if not time_slot:
            raise forms.ValidationError(
                (
                    "Unfortunately the chosen time slot is not available. "
                    "Please choose another one"
                )
            )

        slot_user = TimeSlotUser.objects.create(
            name=name, email=email, phone_number=phone_number
        )
        if (
            time_slot.start_time == start_time
            and time_slot.end_time == end_time
        ):
            # current free time becomes booked time slot
            time_slot.slot_user_id = slot_user.pk
            time_slot.booked = True
            time_slot.save()
            booked_time_slot = time_slot
        elif (
            time_slot.start_time == start_time
            and time_slot.end_time > end_time
        ):
            booked_time_slot = ParkingTimeSlot.objects.create(
                slot_user_id=slot_user.pk,
                slot_date_id=time_slot.slot_date.pk,
                start_time=start_time,
                end_time=end_time,
                booked=True,
            )
            # update current free time slot
            time_slot.start_time = end_time
            time_slot.save()
        elif (
            time_slot.start_time < start_time and time_slot.end_time > end_time
        ):
            booked_time_slot = ParkingTimeSlot.objects.create(
                slot_user_id=slot_user.pk,
                slot_date_id=time_slot.slot_date.pk,
                start_time=start_time,
                end_time=end_time,
                booked=True,
            )
            # create 1 new free time slot
            ParkingTimeSlot.objects.create(
                slot_date_id=time_slot.slot_date.pk,
                start_time=end_time,
                end_time=time_slot.end_time,
            )
            # update current free time slot
            time_slot.end_time = start_time
            time_slot.save()
        elif (
            time_slot.start_time < start_time
            and time_slot.end_time == end_time
        ):
            booked_time_slot = ParkingTimeSlot.objects.create(
                slot_user_id=slot_user.pk,
                slot_date_id=time_slot.slot_date.pk,
                start_time=start_time,
                end_time=end_time,
                booked=True,
            )
            # update current free time slot
            time_slot.end_time = start_time
            time_slot.save()
        else:  # this never happens but it makes easy to read the code
            raise forms.ValidationError(
                "Not able to booke listed time slot. Please try again"
            )

        cleaned_data['booked_time_slot_id'] = booked_time_slot.pk

        return cleaned_data
