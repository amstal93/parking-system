import datetime as dt

import pytest

from parking_slots.lib.time_slots import (
    get_free_parking_slots,
    get_parking_slots_dashboard,
)
from tests.factories.parking import (
    ParkingLotFactory,
    ParkingTimeSlotFactory,
    TimeSlotUserFactory,
)
from tests.factories.users import UserFactory


@pytest.mark.django_db
class TestTimeSlots:
    def setup(self):
        self.lot_owner = UserFactory()

    def test_parking_slots_dashboard_create_dates(self):
        number_of_slots = 3

        ParkingLotFactory.create_batch(
            size=number_of_slots, lot_owner=self.lot_owner
        )
        time_slots = get_parking_slots_dashboard(self.lot_owner.pk)

        assert time_slots.count() == number_of_slots
        assert not time_slots.first().booked

    def test_parking_slots_dashboard_different_statuses(self):
        parking_slot1 = ParkingTimeSlotFactory(
            slot_date__parking_lot__lot_owner_id=self.lot_owner.pk,
            start_time=dt.time(9, 0),
            end_time=dt.time(15, 0),
        )

        slot_user = TimeSlotUserFactory()
        ParkingTimeSlotFactory(
            slot_date=parking_slot1.slot_date,
            slot_user=slot_user,
            start_time=dt.time(15, 0),
            end_time=dt.time(18, 0),
            booked=True,
        )

        time_slots = get_parking_slots_dashboard(self.lot_owner.pk)

        assert time_slots.count() == 2
        assert not time_slots.first().booked
        assert time_slots.last().booked

    def test_parking_slots_create_dates(self):
        number_of_slots = 3

        ParkingLotFactory.create_batch(
            size=number_of_slots, lot_owner=self.lot_owner
        )
        time_slots = get_free_parking_slots(self.lot_owner.pk)

        assert time_slots.count() == 1
        assert not time_slots.first().booked

    def test_parking_slots_get_free_time_slot(self):
        parking_slot1 = ParkingTimeSlotFactory(
            slot_date__parking_lot__lot_owner_id=self.lot_owner.pk,
            start_time=dt.time(9, 0),
            end_time=dt.time(15, 0),
        )

        slot_user = TimeSlotUserFactory()
        ParkingTimeSlotFactory(
            slot_date=parking_slot1.slot_date,
            slot_user=slot_user,
            start_time=dt.time(15, 0),
            end_time=dt.time(18, 0),
            booked=True,
        )

        time_slots = get_free_parking_slots(self.lot_owner.pk)

        assert time_slots.count() == 1
        assert not time_slots.first().booked
